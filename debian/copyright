Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: horae
Source: https://github.com/bruceravel/horae
Files-Excluded: lib/Ifeffit/lib/pod.css
Comment: the CC 2.5 license is not DFSG compatible.

Files: *
Copyright: 2001-2002 Bruce Ravel / 1992-2002 Matt Newville
           1992-2003 Matt Newville, Univ of Chicago
           1998-2010 Bruce Ravel (bravel AT bnl DOT gov).
           2002 University of Washington / 2001-2006 Bruce Ravel / 1992-2006 Matt Newville
           2003 Fachinformationszentrum Karlsruhe, and the U.S. Secretary of 
              Commerce on behalf of the United States.
           2000 - 2001 Stephen O. Lidie.
           2001-2006 http:cars.uchicago.edu/~ravel/software/ / 1992-2006 Matt Newville
           2001-2010 Bruce Ravel <bravel@anl.gov> http://feff.phys.washington.edu/~ravel/software/exafs/
License: horae

Files: debian/*
Copyright: 2021 Neil Williams <codehelp@debian.org>
License: GPL-2+

Files: lib/Xray/atp/atp.el
Copyright: 1998-2006 Bruce Ravel <bravel@anl.gov>
License: Artistic or GPL-2+

Files: attic/language/atomsrc.en attic/language/language.script
 attic/language/tkatomsrc.en attic/scripts/apt.pl attic/scripts/cgi/atoms.cgi
 attic/scripts/check-cpan attic/scripts/dafs.pl attic/scripts/findatp
 attic/scripts/powder.pl attic/scripts/template.pl attic/scripts/Tk/Absorption.pm
 attic/scripts/tkatoms.pl attic/scripts/Tk/Atoms.pm attic/scripts/Tk/Config.pm
 attic/scripts/Tk/Dafs.pm attic/scripts/Tk/Molecule.pm attic/scripts/Tk/Plotter.pm
 attic/scripts/Tk/Powder.pm attic/scripts/Tk/Utils.pm attic/Xtal/language.script
 attic/Xtal/space_groups.db.PL attic/Xtal/Xtal.pm attic/Xtal/xtalrc.en bin/atoms
 Bundle/HoraeBundle.pm install_prereqs lib/Tk/bindDump.pm.save lib/Xray/Atoms.pm
 lib/Xray/ATP.pm lib/Xray/lib/atomsrc.en lib/Xray/lib/xtalrc.en
 lib/Xray/space_groups.db.PL lib/Xray/Tk/SGB.pm lib/Xray/Xtal.pm
Copyright: 1998-2010 Bruce Ravel (bravel AT bnl DOT gov)
           1992-2010 Matt Newville
License: GPL-1+ or Artistic
Comment:
 This program is free software; you can redistribute it and/or modify it
 under the same terms as Perl itself.

License: GPL-1+
 On Debian GNU/Linux systems, the complete text of the GNU General
 Public License can be found in `/usr/share/common-licenses/GPL-1'.

License: Artistic
 On Debian GNU/Linux systems, the complete text of the
 Artistic Licence can be found in `/usr/share/common-licenses/Artistic'.

License: horae
 Athena:  graphical data processing using ifeffit
 Artemis: graphical data analysis using ifeffit
 .
        Athena and Artemis are copyright (c) 2001-2002 Bruce Ravel
                                         ravel@phys.washington.edu
                           http://feff.phys.washington.edu/~ravel/
 .
                  Ifeffit is copyright (c) 1992-2002 Matt Newville
                                        newville@cars.uchicago.edu
                      http://cars9.uchicago.edu/~newville/ifeffit/
 .
   The latest versions of Athena and Artemis can always be found at
     http://feff.phys.washington.edu/~ravel/software/exafs/
 .
 -------------------------------------------------------------------
    All rights reserved. This program is free software; you can
    redistribute it and/or modify it provided that the above notice
    of copyright, these terms of use, and the disclaimer of
    warranty below appear in the source code and documentation, and
    that none of the names of The Naval Research Laboratory, The
    University of Chicago, University of Washington, or the authors
    appear in advertising or endorsement of works derived from this
    software without specific prior written permission from all
    parties.
 .
    THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
    EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES
    OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
    NONINFRINGEMENT.  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
    HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY,
    WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
    FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
    OTHER DEALINGS IN THIS SOFTWARE.
 -------------------------------------------------------------------

License: GPL-2+
 This package is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <https://www.gnu.org/licenses/>
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".
