.TH athena 1 "June 26, 2006"
.SH NAME
athena \- interactive \s-1XAS\s0 data processing
.SH SYNOPSIS
.B athena
.RI " file" ...
.SH DESCRIPTION
This manpage documents the 
.B athena
program, for interactive \s-1XAS\s0 data processing, including
converting raw data to mu(E), aligning, merging, deglitching, Fourier
transforming, and plotting.  Basically, \fBAthena\fR is intended to
handle all chores involving \s-1XAS\s0 data except for quantitative analysis
by fitting to theoretical standards.  \fBAthena\fR's sister program
\&\fBArtemis\fR is the fitting program.
.PP
\&\fBAthena\fR has two interesting and unusual features.  The first is that
there are no buttons for explicitly removing the background from mu(E)
data or for performing Fourier transforms.  The only active buttons
displayed on the main window are for plotting.  \fBAthena\fR always knows
when data requires a background removal or a Fourier transform and
will perform the necessary analysis steps before displaying the plot.
.PP
The second interesting feature is that it is just as easy to perform
analysis and plotting chores on multiple data sets as on an individual
data set.  \fBAthena\fR automates the most common data processing chores
and automatically generates plots of one or more data sets.
.SH "SYNOPSIS"
.IX Header "SYNOPSIS"
\&\fBAthena\fR is a graphical and interactive program written in the perl
programming language, using the Tk display engine, the \s-1IFEFFIT\s0 \s-1EXAFS\s0
library, and the \s-1PGPLOT\s0 plotting library.  (See below for a list of
relevant URLs.)
.PP
When \fBAthena\fR starts, you are presented with a window whose layout
looks something like this:
.PP
.RS
+----------------------------------------+
.br
|    menubar                             |
.br
+---------------------------+------------+
.br
|                           |            |
.br
|                           |            |
.br
|                           |   Group    |
.br
|     Group                 |   List     |
.br
|    Parameters             |            |
.br
|                           |            |
.br
|                           |            |
.br
|                           +------------+
.br
|                           | Plot crrnt |
.br
|                           +------------+
.br
|                           | Plot mrked |
.br
|                           +------------+
.br
|                           |            |
.br
|                           |   Plot     |
.br
|                           |  Features  |
.br
|                           |            |
.br
|                           |            |
.br
+---------------------------+------------+
.br
|    echo area                           |
.br
+----------------------------------------+
.RE
.PP
As you import data into \fBAthena\fR, \fIdata groups\fR are created and
those groups are listed in the section labeled `Group List' .  You
select a group as the active one by clicking the left mouse button on
its entry in the group list.  When selected, the list entry is
highlighted with an orange background and the parameters associated
with that group are displayed in the large area on the left labeled
`Group Parameters'.  When you pass the mouse over a label in the group
parameters section, you will notice that the label changes color.
This indicates that a mouse click on the label will have an effect.
Clicking the left mouse button will display a hint in the echo area as
to the function of that parameter.  See the section on group operations 
for the effect of a right mouse click.
.PP
The view of the group parameters is replaced when certain features of
\&\fBAthena\fR are used.  Choosuing any of the options from the Data,
Align, Diff, or Analysis menus will temporarily replace the group
parameters with views of parameters relevant to the chosen task.  For
example, when the log-ratio option is chosen from the Analysis menu,
the view of group parameters is replaced by a view of the interface to
\&\fBAthena\fR's log\-ratio/phase\-difference analysis.
.PP
Below the group list are two rows of button for plotting data.  The
red buttons are for plotting the selected data group.  The purple
buttons are for plotting multiple data groups.  These buttons are
labeled according to the data space of the plot.  \fIE\fR, \fIk\fR, \fIR\fR,
and \fIq\fR refer to \fIenergy\fR, \fIphotoelectron k\fR, \fIreal space R\fR, and
\&\fIbacktransform k\fR, respectively.
.PP
Below the plot buttons are a set of tabs for specifying the details of
the plots in each space.  For each space you can specify the range of
the x\-axis.  For energy plots, you can select whether the background
function is plotted along with the data and whether the data and
background are normalized.  For k\-space plots, you can select the
amount of k\-weighting.  For R\- and q\-space plots, you can select which
part of those complex functions are plotted.  There are also tabs for
setting up stacked plots and for establishing plot indicators.  For a
complete discussion see the section on plotting.
.PP
Finally, at the bottom of the page is the \fIecho area\fR.  \fBAthena\fR
uses this area to display hints, brief help messages, warnings, and
updates about recently performed analysis or plotting actions.  A few
features of \fBAthena\fR will prompt the user for a text string.  In
those situations, the echo area is temporarily replaced by text string
dialog.
.PP
Throughout \fBAthena\fR the right mouse button serves to post \fIcontext
sensitive menus\fR.  These include parameter labels, groups list
entries, and other elements on the screen.  You should try clicking
the right mouse button in different places to see what usefuls things
might pop up.
.SH "REFERENCES"
.IX Header "REFERENCES"
Here are the relevant URLs:
.IP "\s-1IFEFFIT\s0" 4
.IX Item "IFEFFIT"
.RS
http://cars.uchicago.edu/ifeffit
.RE
.IP "\s-1PGPLOT\s0" 4
.IX Item "PGPLOT"
.RS
http://www.astro.caltech.edu/~tjp/pgplot/
.RE
.IP "Perl" 4
.IX Item "Perl"
.RS
http://www.perl.com
.RE
.IP "perl/Tk" 4
.IX Item "perl/Tk"
.RS
http://www.lehigh.edu/~sol0/ptk/
.RE
.IP "Central atom phase shifts" 4
.IX Item "Central atom phase shifts"
.RS
http://leonardo.phys.washington.edu/~ravel/software/feff_tables/
.RE
.SH "MISSING FEATURES"
.IX Header "MISSING FEATURES"
You betcha!  Lots!  Here's a partial list:
.IP "*" 4
Principal Component Analysis on the set of marked groups
.IP "*" 4
\&\s-1XANES\s0 analysis by fitting a sum of scans to an unknown
.IP "*" 4
Dead-time and self-absorption corrections
.IP "*" 4
Handle dispersive data sets, i.e. data sets wherein the I0 and \s-1IT\s0
measurements are in separate data files becuase the data were obtained
sequentially
.IP "*" 4
Calibrate dispersive data to a trusted standard
.IP "*" 4
Handle \s-1SPEC\s0 files. An independent perl module would be ideal. This is
a project some eager contributor could tackle without having to delve
into \s-1ATHENA\s0's source code.
.IP "*" 4
Alignment of scans using a reference, either of the same edge or of a
nearby edge. (Any suggestions? I am lacking a good idea about how to
implement this...)
.IP "*" 4
Formulas for the energy axis (useful for converting encoder readings
to energy values)
.IP "*" 4
R and q space records, that is to be able to read and write data in R
and q space just as easily as E or k space.
.IP "*" 4
Internationalization. That is, build a framework for having text
strings read from external files and for the language to be a
configuration option.
.IP "*" 4
Documentation, documentation, documentation
.SH "WHAT'S IN A NAME?"
.IX Header "WHAT'S IN A NAME?"
Athena was the goddess of wisdom and is also associated with skill and
justice.  Those are all good qualities for a data analysis program!
.PP
.SH OPTIONS
This program requires no options.
.SH SEE ALSO
This program is part of the horae XAS analysis suite
which can be obtained at 
.br
http://cars9.uchicago.edu/~ravel/software/
.SH "AUTHOR"
.IX Header "AUTHOR"
Bruce Ravel <ravel@phys.washington.edu> (c) 2001 - 2003
.PP
Ifeffit is copyright (c) 1992 - 2003 Matt Newville
.br
newville@cars.uchicago.edu
.br
http://cars9.uchicago.edu/~newville/ifeffit/
