package Ifeffit::Plugins::Filetype::Athena::PFBL12C;  # -*- cperl -*-

=head1 NAME

Ifeffit::Plugin::Filetype::Athena::PFBL12C - filetype plugin for Photon Factory and SPring8

=head1 SYNOPSIS

This plugin converts data recorded as a function of mono angle to data
as a function of energy.

=cut

use vars qw(@ISA @EXPORT @EXPORT_OK);
use Exporter;
use File::Basename;
use File::Copy;
@ISA = qw(Exporter AutoLoader);
@EXPORT_OK = qw();

use vars qw($is_binary $description);
$is_binary = 1;
$description = "Read XAS data from Photon Factory and SPring8.";


use constant PI      => 4 * atan2 1, 1;
use constant HC      => 12398.52;
# use constant HBARC   => 1973.27053324;
# use constant TWODONE => 6.2712;	# Si(111)
# use constant TWODTHR => 3.275;	# Si(311)

=head1 Methods

=over 4

=item C<is>

A Photon Factory XAS file is identified by the string "KEK-PF"
followed by the beamline number in the first line of the file.

=cut


sub is {
  shift;
  my $data = shift;
  open D, $data or die "could not open $data as data (Photon Factory)\n";
  my $line = <D>;
  close D;
  if ($line =~ m{(\#\s+9809).+}i) {
    return 0;
  } elsif ($line =~ m{9809\s+(?:KEK-PF|SPring-8)\s+(?:(BL\d+)|(NW\d+)|(\d+\w+\d*))}i) {
    return 1;
  };
  return 0;
};

=item C<fix>

Convert the wavelength array to energy using the formula

   data.energy = hc / 2D * sin(data.angle)

where C<hc=12398.52> is the the value in eV*angstrom units and D is
the Si(111) plane spacing.

=cut



sub fix {
  shift;
  my ($data, $stash_dir, $top, $r_hash) = @_;
  my ($nme, $pth, $suffix) = fileparse($data);
  my $new = File::Spec->catfile($stash_dir, $nme);
  ($new = File::Spec->catfile($stash_dir, "toss")) if (length($new) > 127);
  open my $D, $data or die "could not open $data as data (fix in PFBL12C)\n";
  open my $N, ">".$new or die "could not write to $new (fix in PFBL12C)\n";

  my $header = 1;
  while (<$D>) {
    next if ($_ =~ m{\A\s*\z});
    last if ($_ =~ m{});
    chomp;
    if ($header and ($_ =~ m{\A\s+offset}i)) {
      my $this = $_;
      print $N '# ', $_, $/;
      print $N '# --------------------------------------------------', $/;
      print $N '# energy_requested   energy_attained  time  i0  i1  ', $/;
      $header = 0;
    } elsif ($header) {
      my $this = $_;
      if ($this =~ m{d=\s+(\d\.\d+)\s+A}i) {
	$ddistance = $1*2;
      };
      print $N '# ', $_, $/;
    } else {
      my @list = split(" ", $_);
      $list[0] = (HC) / ($ddistance * sin($list[0] * PI / 180));
      $list[1] = (HC) / ($ddistance * sin($list[1] * PI / 180));
      my $ndet = $#list-2;
      foreach my $i (1..$ndet) {
	$list[2+$i] = $list[2+$i];
      };
      my $pattern = "  %9.3f  %9.3f  %6.2f" . "  %12.3f" x $ndet . $/;
      printf $N $pattern, @list;
    };
  };
  close $N;
  close $D;
  return $new;
};


=head1 REVISIONS

=over 4

=item *

Thanks to 上村洋平 (Yohéi Uemura) from the Photon Factory for helping
me to refine the C<is> method to work with multiple PF XAS beamlines
as well as XAS data from SPring8.

=back

=head1 AUTHOR

  Bruce Ravel <bravel@anl.gov>
  http://feff.phys.washington.edu/~ravel/software/exafs/
  Athena copyright (c) 2001-2010

=cut



1;
__END__

